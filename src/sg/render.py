import os

from jinja2 import Environment, FileSystemLoader

from sg.config import Config


def render_page(post_data, template, file_path):
    print(f"Rendering {post_data} page to static file.")
    with open(file_path, "w+") as file:
        html = template.render(
            page=post_data,
        )
        file.write(html)


def render_posts(config: Config):
    env = Environment(loader=FileSystemLoader(config.templates_path))
    for post in config.posts:
        file_path = os.path.join(config.config_root_path, post.file_name, ".html")
        render_page(
            post_data=post.dict(),
            template=env.get_template(config.pages.posts.template),
            file_path=file_path,
        )
