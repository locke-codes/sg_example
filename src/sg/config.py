import logging
import os
from pathlib import Path
from shutil import copytree
from typing import Optional, List

from pydantic import PrivateAttr
from pydantic_yaml import YamlModel

logger = logging.Logger(__file__)

PACKAGE_DIR = Path(os.path.abspath(__file__)).parent
ASSETS = os.path.join(PACKAGE_DIR, "assets")
CONFIG_FILE = "config.yaml"
RECURSION_ERROR_MESSAGE = (
    f"Recursion depth exceeded. Please ensure {CONFIG_FILE} exists in project root"
)


def _raise_rec():
    logger.exception(RECURSION_ERROR_MESSAGE)
    raise RecursionError(RECURSION_ERROR_MESSAGE) from None


def _rec(path, depth=0):
    """Dirty method to get calling project root directory based on config.yaml location"""
    if depth > 5:
        _raise_rec()

    if os.path.exists(os.path.join(path, CONFIG_FILE)):
        return path
    try:
        return _rec(Path(path).parent, depth=depth + 1)
    except RecursionError:
        _raise_rec()


def _get_project_root() -> Path:
    return _rec(Path(os.path.abspath(os.path.curdir)))


def _get_project_config(root_path: Path = None) -> Path:
    root_path = root_path or _get_project_root()
    return Path(os.path.join(root_path, CONFIG_FILE))


class Page(YamlModel):
    template: str
    name: Optional[str] = None


class Pages(YamlModel):
    index: Page
    posts: Page


class BodyItem(YamlModel):
    item_type: str
    content: str


class Post(YamlModel):
    title: str
    file_name: str
    location: str
    body: List[BodyItem]


class Config(YamlModel):
    _config_path: str = PrivateAttr(default_factory=_get_project_config)
    _config_root_path: str = PrivateAttr(default_factory=_get_project_root)

    pages: Pages
    posts: List[Post]

    @classmethod
    def load_config(cls) -> "Config":
        project_root = _get_project_root()
        cls.create_default_assets(project_root)
        return cls.parse_file(_get_project_config(project_root), content_type="yaml")

    @classmethod
    def create_default_assets(cls, project_root):
        dest_assets = Path(os.path.join(project_root, "assets"))
        dest_html = Path(os.path.join(project_root, "html"))
        dest_templates = Path(os.path.join(project_root, "templates"))
        if not os.path.exists(dest_assets):
            copytree(ASSETS, dest_assets)
        Path.mkdir(dest_html, parents=True, exist_ok=True)
        Path.mkdir(dest_templates, parents=True, exist_ok=True)

    @staticmethod
    def convert_to_abs_path(path) -> Path:
        return Path(os.path.abspath(path))

    @property
    def config_path(self) -> Path:
        return self.convert_to_abs_path(self._config_path)

    @config_path.setter
    def config_path(self, path):
        self._config_path = path

    @property
    def config_root_path(self) -> Path:
        return self.convert_to_abs_path(self._config_root_path)

    @config_root_path.setter
    def config_root_path(self, path):
        self._config_root_path = path

    @property
    def templates_path(self) -> Path:
        return Path(os.path.join(self.config_root_path, "templates"))
