import logging

import click

from sg.config import Config
from sg.render import render_posts

logger = logging.Logger(__file__)


@click.group()
def cli():
    pass


@cli.command()
def render():
    """
    Render config.yaml pages to html files using jinja templating
    """
    config = Config.load_config()
    render_posts(config=config)


def main():
    cli()
