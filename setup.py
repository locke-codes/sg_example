from setuptools import setup, find_packages

requires = ["Jinja2", "MarkupSafe", "pydantic", "pydantic-yaml", "pyyaml", "click"]

setup(
    name="sg",
    version="1.0",
    package_dir={"": "src"},
    packages=find_packages(where="src"),
    include_package_data=True,
    py_modules=["sg"],
    install_requires=requires,
    entry_points={"console_scripts": ["sg = sg.cli:main"]},
)
